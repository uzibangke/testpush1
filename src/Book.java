
public class Book {
	
	private String title;
	private String releaseDate;
	private Author author;
	private Publisher publisher;
	private int price;
	
	
	public Book () {
		
		
	}
	
	public Book(String title, String releaseDate, Author author, Publisher publisher, int price) {
		super();
		this.title = title;
		this.releaseDate = releaseDate;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public void calculatePriceBook() {
		String rating = getAuthor().getRating();
		int prodactionCost = getPublisher().getProdactionCost();
		
		if (rating.equalsIgnoreCase("Good")) {
			price = (120*prodactionCost)/100;
		}
		else if (rating.equalsIgnoreCase("Excelent")) {
			price = (140*prodactionCost)/100;
		}
		else if (rating.equalsIgnoreCase("Best seller")) {
			price = (150*prodactionCost)/100;
		}
		else {
			price = 0;
			
		}
		
		
		
	}
	
	
	
	public String showInformationBook() {
		return "Book Information : " + "\n" + "\nTitle : " + title + "\nRelease Date : " + releaseDate + "\n" +"\nAuthor Information : " + "\n" + getAuthor().getInformationAuthor() + "\n" + "\nPublisher Information :" +
				"\nPublisher : " + getPublisher().getInformationPublisher() + "\nPrice : " + price;
	}
	
	 

}

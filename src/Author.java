
public class Author {
	
	private String firstName;
	private String lastName;
	private String gender;
	private int age;
	private String country;
	private String rating;
	
	public Author() {
		//INI CONSTRACTOR YA

}
	
	public Author(String firstName, String lastName, String gender, int age, String country, String rating) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.rating = rating;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getFullName() {
		return firstName + " " + lastName;
	}
	public String getInformationAuthor() {
		return "\nAuthor : " + getFullName() + "\nGender : " + gender + " " + age + "\nCountry : " + country + "\nRating : " + rating;
		
	}
	
}
